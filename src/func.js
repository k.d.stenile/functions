const getSum = (str1, str2) => {
  if(typeof str1==='string'||typeof str2==='string'){
    if(str1.length===0){
      str1=0;
    }
    if(str2.length===0){
      str2=0;
    }
    let a=parseInt(str1);
    let b=parseInt(str2);
   
    if(isNaN(a)|| isNaN(b)){
      return false;
    }
    return (a+b).toString();
  }
  return false;
  
};

const executeforEach = (arr, func) => {
  return Array.prototype.map.call(arr,func);
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let countPost=0;
  let countComments=0;
  for (const func of [...listOfPosts]) {
    if(func.author===authorName){
      countPost++;
    }   
  }
  for (const func of [...listOfPosts]) {
    if(func.comments){
      for (const el of func.comments) {
        if(el.author===authorName){
          countComments++;
        }        
      }
    }   
  }
  return "Post:"+countPost+",comments:"+countComments;
};

const tickets=(people)=> {
  let count25=0,
      count50=0,
      count100=0;
      for (const p of people) {
        if (p===25) {
          count25++;         
        }else if(p===50&&count25>0){
          count50++;
          count25--;
        }else if(p===100&&count50>0&&count25>0){
          count100++;
          count50--;
          count25--;
        }else if(p===100&&count25>=3){
          count100++;
          count25-=3;
        }else{return "NO";}
      }
      if(count100>=0&&count50>=0&&count25>=0){
        return "YES";
      }
      return "NO";

};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
